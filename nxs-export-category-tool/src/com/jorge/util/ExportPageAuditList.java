package com.jorge.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by Bill.Deng on 5/31/2017.
 */
public class ExportPageAuditList extends Util {
    private static Connection con = null;
    private static String sourceFilePath;
    private static String resultFilePath;
    private static String userName;
    private static String passWord;
    private static String server;
    private static List<String> nexstarPages = new ArrayList<String>();

    public static void main(String[] args) throws SQLException {
        try {
            System.out.println("start !!!");
            readProperties();
            con = dbConn(userName, passWord, server);
            if (con == null) {
                System.out.print("no connection");
                System.exit(0);
            }
            String rootUrl = "/sections-amino";
            Long stagingGroupId = 60233535L;
            printNexstarPages(stagingGroupId, rootUrl);
            printLakanaPages(stagingGroupId);
            System.out.println("done !!!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    private static void printLakanaPages(Long stagingGroupId) {
        String sql = "select EXTRACTVALUE(XMLTYPE(name), '/root/Name/text()') " +
                "name,friendlyurl from layout where friendlyurl like '%-amino' " +
                "and groupid=" + stagingGroupId +  "  and Privatelayout=0";

        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String pageName = rs.getString("name");
                String pageUrl = rs.getString("friendlyurl");
                if( ! nexstarPages.contains(pageUrl)) {
                    System.out.println(pageName + "\t" + pageUrl + "\t" + "LAKANA");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {

                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void printNexstarPages(Long stagingGroupId, String rootUrl) {
        String sql = "select EXTRACTVALUE(XMLTYPE(name), '/root/Name/text()') " +
                "name,layoutid,friendlyurl from layout where friendlyurl='" +
                rootUrl +  "' and groupid=" + stagingGroupId + " and Privatelayout=0";

        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String pageName = rs.getString("name");
                Long layoutId = rs.getLong("layoutid");
                String pageUrl = rs.getString("friendlyurl");
                System.out.println(pageName + "\t" + pageUrl + "\t" + "Nexstar");
                nexstarPages.add(pageUrl);
                printChildPages(stagingGroupId, layoutId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {

                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private static void printChildPages(Long stagingGroupId, Long parentLayoutId) {
        String sql = "select EXTRACTVALUE(XMLTYPE(name), '/root/Name/text()') " +
                "name,layoutid,friendlyurl from layout where parentlayoutid=" +
                parentLayoutId + " and groupid=" + stagingGroupId +
                " and Privatelayout=0 order by priority";
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String pageName = rs.getString("name");
                Long layoutId = rs.getLong("layoutid");
                String pageUrl = rs.getString("friendlyurl");
                System.out.println(pageName + "\t" + pageUrl + "\t" + "Nexstar");
                nexstarPages.add(pageUrl);
                printChildPages(stagingGroupId, layoutId);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {

                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


    public static void readProperties() throws IOException {
        Properties prop = new Properties();
        InputStream in = Util.class.getResourceAsStream("/project.properties");
        try {
            prop.load(in);
            sourceFilePath = prop.getProperty("sourceFilePath").trim();
            resultFilePath = prop.getProperty("resultFilePath").trim();
            userName = prop.getProperty("userName").trim();
            passWord = prop.getProperty("passWord").trim();
            server = prop.getProperty("server").trim();
        } catch (IOException e) {
            // Writes in log.
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // Writes in log.
                }
            }
        }
    }

}
