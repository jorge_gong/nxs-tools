package com.jorge.util;
import java.util.List;


public class PageCategoryBean {
	private String pageUrl;
	private String categoryName;
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public PageCategoryBean(String pageUrl, String categoryName) {
		super();
		this.pageUrl = pageUrl;
		this.categoryName = categoryName;
	}
	
}
