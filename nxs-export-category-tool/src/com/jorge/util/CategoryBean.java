package com.jorge.util;

public class CategoryBean {
	private String pageName;
	private String parentCategory;
	private String child1Category;
	private String child2Category;
	private String groupName;
	
	public String getPageName() {
		return pageName;
	}
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	public String getChild1Category() {
		return child1Category;
	}
	public void setChild1Category(String child1Category) {
		this.child1Category = child1Category;
	}
	public String getChild2Category() {
		return child2Category;
	}
	public void setChild2Category(String child2Category) {
		this.child2Category = child2Category;
	}
}
