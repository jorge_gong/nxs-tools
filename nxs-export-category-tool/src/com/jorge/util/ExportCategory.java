package com.jorge.util;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ExportCategory extends Util {
    private static Connection con = null;
    private static Statement state = null;
    private static ResultSet rs = null;
    private static List<PageCategoryBeans> pageCategoryBeanList;
    private static List<CategoryBean> categoryBeanList;
    private static String sourceFilePath;
    private static String resultFilePath;
    private static String groupId;
    private static String userName;
    private static String passWord;
    private static String server;
    private static String preParentCategory;
    private static String preChild1Category;

    public static void main(String[] args) throws SQLException {
        pageCategoryBeanList = new ArrayList<PageCategoryBeans>();
        preParentCategory = "";
        preChild1Category = "";
        try {
            System.out.println("start !!!");
            readProperties();
            con = dbConn(userName, passWord, server);
            if (con == null) {
                System.out.print("no connection");
                System.exit(0);
            }

            categoryBeanList = importCategpory(sourceFilePath, 0);
            float k = categoryBeanList.size();
            for (CategoryBean categoryBean : categoryBeanList) {
                PageCategoryBeans pageCategoryBean = queryPageCategory(categoryBean);
                pageCategoryBeanList.add(pageCategoryBean);
                float i = pageCategoryBeanList.size();
                float progress = i / k * 100;
                String str = Float.toString(progress);
                str = str.substring(0, str.indexOf(".")) + "%";
                System.out.print("\r");
                System.out.print(str);
            }

            exportCategory(resultFilePath, pageCategoryBeanList);

            System.out.println("done !!!");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (state != null) {
                state.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

    private static PageCategoryBeans queryPageCategory(CategoryBean categoryBean) {
        PageCategoryBeans pageCategoryBean = new PageCategoryBeans();
        String pageName = categoryBean.getPageName();
        String parentCategory = categoryBean.getParentCategory();
        String child1Category = categoryBean.getChild1Category();
        String child2Category = categoryBean.getChild2Category();
        String groupName = categoryBean.getGroupName();
        if (groupId == null && !groupName.equals("")) {
            groupId = queryGroupId(groupName);
        }
        if (groupId != null) {
            pageCategoryBean.setGroupName(groupName);
        }
        if (!pageName.equals("")) {
            String pageUrl = queryPageUrl(pageName, groupId);
            pageCategoryBean.setPageUrl(pageUrl);
        } else {
            pageCategoryBean.setPageUrl(null);
        }

        // the category is parent category
        if (!parentCategory.equals("")) {
            // select global category
            pageCategoryBean.setCategoryName(parentCategory);
            String globalCategory = queryParentCategory(parentCategory, "10187");
            if (globalCategory != null && globalCategory.contains("category_deprecated")) {
                globalCategory = globalCategory.replace("category_deprecated", "");
            }
            // store pre parent category
            preParentCategory = globalCategory;
            if (globalCategory != null) {
                //check site category exist
                String siteCategory = queryParentCategory(parentCategory, groupId);
                if (siteCategory == null) {
                    //If site category is not exist, we need add the category record to site.
                    pageCategoryBean.setSiteCategory(globalCategory);
                } else {
                    if (siteCategory.contains("category_deprecated")) {
                        pageCategoryBean.setSiteCategory(siteCategory.replace("category_deprecated", ""));
                    }
                }
            } else {
                //If global category is not exist, we need add the category record to global and site.
                pageCategoryBean.setGlobalCategory(parentCategory);
                pageCategoryBean.setSiteCategory(parentCategory);
            }
        } else {
            //the category is child category
            if (!child1Category.equals("")) {
                pageCategoryBean.setCategoryName(child1Category.concat("|"));
                String siteChild1Category = queryChildCategory(child1Category, groupId, preParentCategory);
                if (siteChild1Category != null && siteChild1Category.contains("category_deprecated")) {
                    preChild1Category = siteChild1Category.replace("category_deprecated", "");
                } else {
                    preChild1Category = siteChild1Category;
                }
                if (siteChild1Category == null) {
                    //If site child category is note exist, we need add the category record to site.
                    pageCategoryBean.setSiteCategory(preParentCategory + "|" + child1Category);
                    //Check to see if the global exists in this category.
                    String globalChild1Category = queryChildCategory(child1Category, "10187", preParentCategory);
                    if (globalChild1Category == null) {
                        //If global child category is note exist, we need add the category record to global.
                        pageCategoryBean.setGlobalCategory(preParentCategory + "|" + child1Category);
                    }
                } else {
                    if (siteChild1Category.contains("category_deprecated")) {
                        pageCategoryBean.setSiteCategory(preParentCategory + "|" + siteChild1Category.replace("category_deprecated", ""));
                    }
                }
            } else {
                if (!child2Category.equals("")) {
                    pageCategoryBean.setCategoryName(child2Category.concat(";"));
                    String siteChild2Category = queryChildCategory(child2Category, groupId, preChild1Category);
                    if (siteChild2Category == null) {
                        //If site child category is note exist, we need add the category record to site.
                        pageCategoryBean.setSiteCategory(preParentCategory + "|" + preChild1Category + "|" + child2Category);
                        //Check to see if the global exists in this category.
                        String globalChild2Category = queryChildCategory(child2Category, "10187", preChild1Category);
                        if (globalChild2Category == null) {
                            //If global child category is note exist, we need add the category record to global.
                            pageCategoryBean.setGlobalCategory(preParentCategory + "|" + preChild1Category + "|" + child2Category);
                        }
                    } else {
                        if (siteChild2Category.contains("category_deprecated")) {
                            pageCategoryBean.setSiteCategory(preParentCategory + "|" + preChild1Category + "|" + siteChild2Category.replace("category_deprecated", ""));
                        }
                    }
                }
            }
        }
        return pageCategoryBean;
    }


    private static String queryChildCategory(String childCategory,
                                             String groupId, String preParentCategory2) {
        String siteChildCategory = null;
        String categoryId = null;
        String childCategoryName;
        String parentCategoryName;
        String sql = " select category2.name, category1.name, category1.categoryid from assetcategory" +
                " category1 inner join assetcategory category2 on category1.parentcategoryid = category2.categoryid " +
                " where lower(category1.NAME) = '" + childCategory + "' and " +
                " category1.groupid = " + groupId + " and category2.groupid =" + groupId;
        try {
            state = con.createStatement();
            rs = state.executeQuery(sql);
            while (rs.next()) {
                parentCategoryName = rs.getString(1);
                childCategoryName = rs.getString(2);
                categoryId = rs.getString(3);
                //check parent category is same with pre.
                if (parentCategoryName.equals(preParentCategory2)) {
                    siteChildCategory = childCategoryName;
                    String deprecated_sql = "select value from assetcategoryproperty where categoryid = '" + categoryId + "'";
                    rs = state.executeQuery(deprecated_sql);
                    while (rs.next()) {
                        String categoryDeprecatedValue = rs.getString(1);
                        if (categoryDeprecatedValue.equals("category_deprecated")) {
                            siteChildCategory += "category_deprecated";
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return siteChildCategory;
    }

    private static String queryParentCategory(String parentCategory,
                                              String groupId) {
        String siteCategory = null;
        String sql = "select name, value from assetcategory" +
                " inner join assetcategoryproperty on assetcategory.categoryid=assetcategoryproperty.categoryid where lower(NAME) = '" + parentCategory
                + "' and groupid = " + groupId + " and parentcategoryid=0";
        try {
            state = con.createStatement();
            rs = state.executeQuery(sql);
            while (rs.next()) {
                siteCategory = rs.getString(1);
                String categoryDeprecatedValue = rs.getString(2);
                if (categoryDeprecatedValue.equals("category_deprecated")) {
                    siteCategory += "category_deprecated";
                    return siteCategory;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return siteCategory;
    }


    private static String queryGroupId(String groupName) {
        String groupId = null;
        String sql = "SELECT group_.groupid from group_ inner join organization_ on organization_.organizationid = group_.classpk"
                + " where organization_.name= '" + groupName + "'";
        try {
            state = con.createStatement();
            rs = state.executeQuery(sql);
            while (rs.next()) {
                groupId = rs.getString(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return groupId;
    }

    private static String queryPageUrl(String pageName, String groupId) {
        String friendlyUrl;
        String pageType;
        String pageUrl = null;

        String sql = " select friendlyurl,type_ from layout " +
                " where lower(EXTRACTVALUE(XMLTYPE(name), '/root/Name/text()')) = " +
                " '" + pageName + "' and " +
                " groupid=" + groupId;
        try {
            state = con.createStatement();
            rs = state.executeQuery(sql);
            while (rs.next()) {
                friendlyUrl = rs.getString(1);
                pageType = rs.getString(2);
                if ("portlet".equalsIgnoreCase(pageType)) {
                    pageUrl = friendlyUrl;
                    return pageUrl;
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return pageUrl;
    }

    public static void readProperties() throws IOException {
        Properties prop = new Properties();
        InputStream in = Util.class.getResourceAsStream("/project.properties");
        try {
            prop.load(in);
            sourceFilePath = prop.getProperty("sourceFilePath").trim();
            resultFilePath = prop.getProperty("resultFilePath").trim();
            userName = prop.getProperty("userName").trim();
            passWord = prop.getProperty("passWord").trim();
            server = prop.getProperty("server").trim();
        } catch (IOException e) {
            // Writes in log.
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    // Writes in log.
                }
            }
        }
    }
}
