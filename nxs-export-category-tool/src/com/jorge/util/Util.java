package com.jorge.util;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

public class Util {
	    public static Connection dbConn(String name, String pass , String server) {
	       Connection conn = null;
	       try {
	           Class.forName("oracle.jdbc.driver.OracleDriver");
	       } catch (ClassNotFoundException e) {
	           e.printStackTrace();
	       }
	       try {
	           conn = DriverManager.getConnection(server, name, pass);
	       } catch (SQLException e) {
	           e.printStackTrace();
	       }
	       return conn;
	    }
	    
	    public static void exportCategory(String filePath, List<PageCategoryBeans> PageCategoryBeanList) {
	        Workbook wb = null;
	        OutputStream out = null;
	        try {
	            wb = new HSSFWorkbook();
	            Sheet sheet = wb.createSheet("page category");
	            sheet.setColumnWidth(0, 18 * 256);
	            sheet.setColumnWidth(1, 18 * 256);
	            int rowIndex = 0;
	            Row r  = sheet.createRow(rowIndex);
	            rowIndex ++;
	            r.createCell(0).setCellValue("Global category");
	            r.createCell(1).setCellValue("Site category");
	            r.createCell(2).setCellValue("Category name");
	            r.createCell(3).setCellValue("Page url");
	            r.createCell(4).setCellValue("Group name");
	            if(PageCategoryBeanList != null) {
	                for(PageCategoryBeans pageCategoryBean : PageCategoryBeanList) {
	                    r = sheet.createRow(rowIndex);
	                    rowIndex ++;
	                    r.createCell(0).setCellValue(pageCategoryBean.getGlobalCategory());
	                    r.createCell(1).setCellValue(pageCategoryBean.getSiteCategory());
	                    r.createCell(2).setCellValue(pageCategoryBean.getCategoryName());
	                    r.createCell(3).setCellValue(pageCategoryBean.getPageUrl());
	                    r.createCell(4).setCellValue(pageCategoryBean.getGroupName());
	                }
	            }

	            out = new FileOutputStream(filePath);
	            wb.write(out);
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            try {
	                out.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	    
	    public static List<CategoryBean> importCategpory(String file, int sheetIndex) throws IOException {
	        FileInputStream in = null;
	        List<CategoryBean> result = null;
	        try {
	            in = new FileInputStream(file);
	            result = new ArrayList<CategoryBean>();
	            Workbook wb = new HSSFWorkbook(in);
	            Sheet sheet = wb.getSheetAt(sheetIndex);
	            for (Row row : sheet) {
	                if (row.getRowNum() < 1) {
	                    continue;
	                }
	                if (row.getCell(1) == null && row.getCell(2) == null && row.getCell(3) == null) {
	                    continue;
	                }
	                
	                if (row.getCell(0) == null) {
	                	return result;
	                }
	                CategoryBean categoryBean = new CategoryBean();
	                categoryBean.setPageName(row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue().trim().toLowerCase());
	                categoryBean.setParentCategory(row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue().trim().toLowerCase());
	                categoryBean.setChild1Category(row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue().trim().toLowerCase());
	                categoryBean.setChild2Category(row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue().trim().toLowerCase());
	                categoryBean.setGroupName(row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue().trim());
	                result.add(categoryBean);
	            }
	        } catch (Exception e) {
	            e.printStackTrace();
	        } finally {
	            in.close();
	        }
	        return result;
	    }
}
