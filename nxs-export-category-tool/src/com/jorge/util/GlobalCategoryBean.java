package com.jorge.util;

public class GlobalCategoryBean {
	private String groupName;
	private String name;
	private String parentCategory;
	private String Vocabulary;
	
	public GlobalCategoryBean(String groupName, String name,
			String parentCategory, String vocabulary) {
		this.groupName = groupName;
		this.name = name;
		this.parentCategory = parentCategory;
		Vocabulary = vocabulary;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	public String getVocabulary() {
		return Vocabulary;
	}
	public void setVocabulary(String vocabulary) {
		Vocabulary = vocabulary;
	}
	
}
