package com.jorge.util;

public class PageCategoryBeans {
	private String globalCategory;
	private String siteCategory;
	private String categoryName;
	private String pageUrl;
	private String groupName;
	public String getGlobalCategory() {
		return globalCategory;
	}
	public void setGlobalCategory(String globalCategory) {
		this.globalCategory = globalCategory;
	}
	public String getSiteCategory() {
		return siteCategory;
	}
	public void setSiteCategory(String siteCategory) {
		this.siteCategory = siteCategory;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getPageUrl() {
		return pageUrl;
	}
	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
	
}
