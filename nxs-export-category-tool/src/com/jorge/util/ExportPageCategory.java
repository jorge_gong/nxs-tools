package com.jorge.util;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.*;

/**
 * Created by Bill.Deng on 2/9/2017.
 */
public class ExportPageCategory {
	private static String globalCategoryFilePath = "";
	private static String siteCategoryFilePath = "";
	private static String blackPageListFilePath = "";
	private static String sourceFilePath = "";

	public static void main(String[] args) throws IOException {
		System.out.println("start !!!");
		readProperties();
		List<SourceBean> sourceBeanList = importExcel(sourceFilePath);
		List<String> blackPageList = importBlackListPageExcel(blackPageListFilePath);
		checkSourceBean(sourceBeanList, blackPageList);
		List<GlobalCategoryBean> globalCategoryBeanList = transformGlobalCategory(sourceBeanList);
		List<SiteCategoryBean> siteCategoryBeanList = transformSiteCategory(sourceBeanList);
		List<PageCategoryBean> pageCategoryBeanList = transformPageCategory(sourceBeanList);
		exportGlobalCategoryLayout(globalCategoryFilePath, globalCategoryBeanList);
		exportSiteCategoryLayout(siteCategoryFilePath, siteCategoryBeanList);
		exportPageCategoryFilePathLayout( pageCategoryBeanList);
		System.out.println("done !!!");
	}

	private static void checkSourceBean(List<SourceBean> sourceBeanList,
										List<String> blackPageList) {
		for (String blackPage : blackPageList) {
			Iterator<SourceBean> it = sourceBeanList.iterator();
			while(it.hasNext()){
				SourceBean sourceBean = it.next();
				if (blackPage.equals(sourceBean.getPageUrl())) {
					it.remove();
				}
			}
		}
	}

	private static List<String> importBlackListPageExcel(
			String blackPageListFilePath) {
		FileInputStream in = null;
		List<String> result = null;
		try {
			in = new FileInputStream(blackPageListFilePath);
			result = new ArrayList<String>();
			Workbook wb = new HSSFWorkbook(in);
			Sheet sheet = wb.getSheetAt(0);
			for (Row row : sheet) {
				if (row.getRowNum() < 1) {
					continue;
				}
				if (row.getCell(0) == null) {
					return result;
				}
				String blackList = row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue();
				result.add(blackList);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	private static void exportPageCategoryFilePathLayout(
			List<PageCategoryBean> pageCategoryBeanList) {
		StringBuilder pageCategoryNameString = new StringBuilder();
		StringBuilder pageUrlString = new StringBuilder();
		for (PageCategoryBean pageCategoryBean : pageCategoryBeanList) {
			String categoryName = pageCategoryBean.getCategoryName();
			String pageUrl = pageCategoryBean.getPageUrl();
			pageCategoryNameString.append("'" + categoryName + "',");
			pageUrlString.append("'"+ pageUrl + "',");
		}
		String pageCategoryString = pageCategoryNameString.substring(0,pageCategoryNameString.length()-1);
		String pageUrl = pageUrlString.substring(0, pageUrlString.length()-1);
		System.out.print("SET SERVEROUTPUT ON;\n" +
				"\n" +
				"DECLARE\n" +
				"    LAYOUT_FRIENDLYURL_KTVX TABLE_ := TABLE_(");
		System.out.print(pageUrl.toLowerCase());
		System.out.print(");\n" +
				"    CATEGORY_LIST_KTVX TABLE_ := TABLE_(");
		System.out.print(pageCategoryString.toLowerCase());
		System.out.println(");\n" +
				"    STAGING_GROUPID_KTVX NUMBER := 60044176;\n" +
				"\n" +
				"BEGIN\n" +
				"  CONNECT_PAGE_WITH_CATEGORY(LAYOUT_FRIENDLYURL_KTVX, CATEGORY_LIST_KTVX, STAGING_GROUPID_KTVX);\n" +
				"  COMMIT;\n" +
				"EXCEPTION WHEN OTHERS THEN\n" +
				"  DBMS_OUTPUT.PUT_LINE('Error Encountered :' || SQLERRM(SQLCODE));\n" +
				"  ROLLBACK;\n" +
				"END;\n" +
				"/");
	}

	private static void exportSiteCategoryLayout(String siteCategoryFilePath,
												 List<SiteCategoryBean> siteCategoryBeanList) {
		Workbook wb = new HSSFWorkbook() ;
		OutputStream out = null;
		try {
			HSSFCellStyle font_bold_style = (HSSFCellStyle) wb.createCellStyle();
			// We will now specify a background cell color
			HSSFFont createFont = (HSSFFont)wb.createFont();
//        	createFont.setBold(true);
//        	font_bold_style.setFont(createFont);

			Sheet sheet = wb.createSheet("Category");
			sheet.setColumnWidth(0, 18 * 256);
			sheet.setColumnWidth(1, 18 * 256);
			int rowIndex = 0;
			Row r = sheet.createRow(rowIndex);
			rowIndex ++;
			// Generate Layout in the first row
			r.createCell(0).setCellValue("Category");
//            r.getCell(0).setCellStyle(font_bold_style);

			//Generate a blank row
			r = sheet.createRow(rowIndex);
			rowIndex ++;

			// Generate Column Title
			r = sheet.createRow(rowIndex);
			rowIndex ++;
			r.createCell(0).setCellValue("Group Name");
			r.createCell(1).setCellValue("Name");
			r.createCell(2).setCellValue("Parent Category");
			r.createCell(3).setCellValue("Vocabulary");
			r.createCell(4).setCellValue("Global Category");
			Iterator<Cell> cellIterator = r.cellIterator();
//            while (cellIterator.hasNext()) {
//                Cell cell = cellIterator.next();
//                cell.setCellStyle(font_bold_style);
//            }

			if(siteCategoryBeanList != null) {
				for(SiteCategoryBean siteCategoryBean : siteCategoryBeanList) {
					r = sheet.createRow(rowIndex);
					rowIndex ++;
					r.createCell(0).setCellValue(siteCategoryBean.getGroupName());
					r.createCell(1).setCellValue(siteCategoryBean.getName());
					r.createCell(2).setCellValue(siteCategoryBean.getParentCategory());
					r.createCell(3).setCellValue(siteCategoryBean.getVocabulary());
					r.createCell(4).setCellValue(siteCategoryBean.getGlobalCategory());
				}
			}
//            File file = new File(siteCategoryFilePath);
			out = new FileOutputStream(siteCategoryFilePath);
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void exportGlobalCategoryLayout(String globalCategoryFilePath,
												   List<GlobalCategoryBean> globalCategoryBeanList) {
		Workbook wb = new HSSFWorkbook(); ;
		OutputStream out = null;
		try {
			HSSFCellStyle font_bold_style = (HSSFCellStyle) wb.createCellStyle();
			// We will now specify a background cell color
			HSSFFont createFont = (HSSFFont)wb.createFont();
			createFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
//        	createFont.setBold(true);
        	font_bold_style.setFont(createFont);

			wb = new HSSFWorkbook();
			Sheet sheet = wb.createSheet("Category");
			sheet.setColumnWidth(0, 18 * 256);
			sheet.setColumnWidth(1, 18 * 256);
			int rowIndex = 0;
			Row r = sheet.createRow(rowIndex);
			rowIndex ++;
			// Generate Layout in the first row
			r.createCell(0).setCellValue("Category");
			Iterator<Cell> cellIterator = r.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
//                cell.setCellStyle(font_bold_style);
            }

			//Generate a blank row
			r = sheet.createRow(rowIndex);
			rowIndex ++;

			// Generate Column Title
			r = sheet.createRow(rowIndex);
			rowIndex ++;
			r.createCell(0).setCellValue("Group Name");
			r.createCell(1).setCellValue("Name");
			r.createCell(2).setCellValue("Parent Category");
			r.createCell(3).setCellValue("Vocabulary");
//			Iterator<Cell> cellIterator = r.cellIterator();
//            while (cellIterator.hasNext()) {
//                Cell cell = cellIterator.next();
//                cell.setCellStyle(font_bold_style);
//            }

			if(globalCategoryBeanList != null) {
				for(GlobalCategoryBean globalCategoryBean : globalCategoryBeanList) {
					r = sheet.createRow(rowIndex);
					rowIndex ++;
					r.createCell(0).setCellValue(globalCategoryBean.getGroupName());
					r.createCell(1).setCellValue(globalCategoryBean.getName());
					r.createCell(2).setCellValue(globalCategoryBean.getParentCategory());
					r.createCell(3).setCellValue(globalCategoryBean.getVocabulary());
				}
			}

//            File file = new File(globalCategoryFilePath);
			out = new FileOutputStream(globalCategoryFilePath);
			wb.write(out);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static List<PageCategoryBean> transformPageCategory(
			List<SourceBean> sourceBeanList) {
		List<PageCategoryBean> pageCategoryBeanList = new ArrayList<PageCategoryBean>();
		String categoryName = "";
		String pageUrl = "";
		for (SourceBean sourceBean : sourceBeanList) {
			categoryName = sourceBean.getCategoryName().trim();
			pageUrl = sourceBean.getPageUrl().trim();
			PageCategoryBean pageCategoryBean = new PageCategoryBean(pageUrl, categoryName);
			pageCategoryBeanList.add(pageCategoryBean);
		}
		return pageCategoryBeanList;
	}

	private static List<SiteCategoryBean> transformSiteCategory(
			List<SourceBean> sourceBeanList) {
		List<SiteCategoryBean> siteCategoryBeanList = new ArrayList<SiteCategoryBean>();
		String parentCategory = "";
		String name = "";
		String globalCategory = "";
		String groupName = "";
		for (SourceBean sourceBean : sourceBeanList) {
			globalCategory = sourceBean.getSiteCategory().trim();
			if (!globalCategory.equals("")) {
				groupName = sourceBean.getGroupName().trim();
				if (globalCategory.contains("|")){
					parentCategory = globalCategory.substring(0,globalCategory.lastIndexOf("|")).trim();
					name = globalCategory.substring(globalCategory.lastIndexOf("|")+1,globalCategory.length()).trim();
				} else {
					parentCategory = "";
					name = globalCategory.trim();
				}
				parentCategory = stringToUpperCase(parentCategory);
				name = stringToUpperCase(name);
				if (!parentCategory.equals("")) {
					globalCategory = parentCategory + "|" + name;
				} else {
					globalCategory = name;
				}
				SiteCategoryBean siteCategoryBean = new SiteCategoryBean(groupName, name, parentCategory, "Content", globalCategory);
				siteCategoryBeanList.add(siteCategoryBean);
			}
		}
		return siteCategoryBeanList;
	}

	private static List<GlobalCategoryBean> transformGlobalCategory(
			List<SourceBean> sourceBeanList) {
		List<GlobalCategoryBean> globalCategoryBeanList = new ArrayList<GlobalCategoryBean>();
		String parentCategory = "";
		String name = "";
		for (SourceBean sourceBean : sourceBeanList) {
			String globalCategory = sourceBean.getGlobalCategory().trim();
			if (!globalCategory.equals("")) {
				if (globalCategory.contains("|")){
					parentCategory = globalCategory.substring(0,globalCategory.lastIndexOf("|")).trim();
					name = globalCategory.substring(globalCategory.lastIndexOf("|")+1,globalCategory.length()).trim();
				} else {
					parentCategory = "";
					name = globalCategory.trim();
				}
				parentCategory = stringToUpperCase(parentCategory);
				name = stringToUpperCase(name);
				GlobalCategoryBean globalCategoryBean = new GlobalCategoryBean("Global", name, parentCategory, "Content");
				globalCategoryBeanList.add(globalCategoryBean);
			}
		}
		return globalCategoryBeanList;
	}

	private static void readProperties() throws IOException {
		Properties prop = new Properties();
		InputStream in = ExportPageCategory.class.getResourceAsStream("/project.properties");
		try {
			prop.load(in);
			sourceFilePath = prop.getProperty("resultFilePath").trim();
			globalCategoryFilePath = prop.getProperty("globalCategoryFilePath").trim();
			siteCategoryFilePath = prop.getProperty("siteCategoryFilePath").trim();
			blackPageListFilePath = prop.getProperty("blackPageListFilePath").trim();
		} catch (IOException e) {
			// Writes in log.
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (IOException e) {
					// Writes in log.
				}
			}
		}
	}

	private static String stringToUpperCase(String str) {
		String finalstr="";
		if (!str.equals("")) {
			String[] ele = str.split(" ");
			for(int i=0;i<ele.length;i++){
				ele[i]=ele[i].substring(0, 1).toUpperCase()+ele[i].substring(1);
				finalstr=finalstr+ele[i]+" ";
			}
			finalstr=finalstr.substring(0, finalstr.length()-1);
		}
		return finalstr;
	}

	public static List<SourceBean> importExcel(String file, int sheetIndex) throws IOException {
		FileInputStream in = null;
		List<SourceBean> result = null;
		try {
			in = new FileInputStream(file);
			result = new ArrayList<SourceBean>();
			Workbook wb = new HSSFWorkbook(in);
			Sheet sheet = wb.getSheetAt(sheetIndex);
			for (Row row : sheet) {
				if (row.getRowNum() < 1) {
					continue;
				}
				if (row.getCell(4) == null) {
					return result;
				}
				SourceBean sourceBean = new SourceBean();
				sourceBean.setGlobalCategory(row.getCell(0) == null ? "" : row.getCell(0).getStringCellValue());
				sourceBean.setSiteCategory(row.getCell(1) == null ? "" : row.getCell(1).getStringCellValue());
				sourceBean.setCategoryName(row.getCell(2) == null ? "" : row.getCell(2).getStringCellValue());
				sourceBean.setPageUrl(row.getCell(3) == null ? "" : row.getCell(3).getStringCellValue());
				sourceBean.setGroupName(row.getCell(4) == null ? "" : row.getCell(4).getStringCellValue());
				result.add(sourceBean);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			in.close();
		}
		return result;
	}

	public static List<SourceBean> importExcel(String file) throws IOException {
		return importExcel(file, 0);
	}

}